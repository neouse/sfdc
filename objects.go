package sfdc

import (
	"fmt"
	"net/http"
)

// Response is returned from most operations involving Salesforce sobjects
type Response struct {
	ID      string        `json:"id"`
	Success bool          `json:"success"`
	Errors  []interface{} `json:"errors"`
}

// GetObjectByID returns the sobject of type name given it's id
func GetObjectByID(client *Client, name, id string) (interface{}, error) {
	var object interface{}
	req, err := client.NewRequest(http.MethodGet, fmt.Sprintf("/services/data/v46.0/sobjects/%s/%s", name, id), &object)
	if err != nil {
		return nil, err
	}

	if err = client.Do(req, nil); err != nil {
		return nil, err
	}

	return object, nil
}

// UpdateObjectByID updates the sobject of type name by id
func UpdateObjectByID(client *Client, name, id string, object interface{}) error {
	req, err := client.NewRequest(http.MethodPatch, fmt.Sprintf("/services/data/v46.0/sobjects/%s/%s", name, id), object)
	if err != nil {
		return err
	}

	return client.Do(req, nil)
}

// CreateObject creates a new sobject of type name with the contents of object
func CreateObject(client *Client, name string, object interface{}) error {
	req, err := client.NewRequest(http.MethodPost, fmt.Sprintf("/services/data/v46.0/sobjects/%s", name), object)
	if err != nil {
		return err
	}

	return client.Do(req, &object)
}
