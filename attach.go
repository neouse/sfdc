package sfdc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"net/url"
)

type documentQueryResponse struct {
	TotalSize int  `json:"totalSize"`
	Done      bool `json:"done"`
	Records   []struct {
		Attributes struct {
			Type string `json:"type"`
			URL  string `json:"url"`
		} `json:"attributes"`
		ContentDocumentID string `json:"ContentDocumentId"`
	} `json:"records"`
}

func findContentDocument(client *Client, docID string) (res *documentQueryResponse, err error) {
	var req *http.Request
	query := fmt.Sprintf("SELECT+ContentDocumentId+FROM+ContentVersion+WHERE+Id+=+'%s'", docID)
	req, err = client.NewRequest(http.MethodGet, fmt.Sprintf("/services/data/v46.0/query/?q=%s", query), nil)
	if err != nil {
		return
	}

	res = &documentQueryResponse{}
	err = client.Do(req, res)
	return
}

func linkContentDocument(client *Client, targetID, docID string) (res *Response, err error) {
	salesforceLink := struct {
		ContentDocumentID string `json:"ContentDocumentId"`
		LinkedEntityID    string `json:"LinkedEntityId"`
		ShareType         string `json:"ShareType"`
	}{
		ContentDocumentID: docID,
		LinkedEntityID:    targetID,
		ShareType:         "V",
	}

	var req *http.Request
	if req, err = client.NewRequest(http.MethodPost, "/services/data/v46.0/sobjects/ContentDocumentLink", salesforceLink); err != nil {
		return
	}

	res = &Response{}
	err = client.Do(req, res)
	return
}

// AttachFile creates a document in the Salesforce environment at the baseURL, attaches the document to the targetId sobject, names the document filename and reads the data for the document from data
func AttachFile(client *Client, targetID, filename string, data io.Reader) (res *Response, err error) {
	var uploadResponse *Response
	uploadResponse, err = upload(client, filename, data)
	if err != nil {
		return
	}

	var documentQuery *documentQueryResponse
	documentQuery, err = findContentDocument(client, uploadResponse.ID)
	if err != nil {
		return
	}
	if len(documentQuery.Records) != 1 || !documentQuery.Done || documentQuery.TotalSize != 1 {
		return
	}

	res, err = linkContentDocument(client, targetID, documentQuery.Records[0].ContentDocumentID)
	return
}

func upload(client *Client, filename string, data io.Reader) (res *Response, err error) {
	var buffer bytes.Buffer
	writer := multipart.NewWriter(&buffer)
	var formWriter io.Writer
	if formWriter, err = writer.CreateFormFile("VersionData", filename); err != nil {
		return
	}
	if _, err = io.Copy(formWriter, data); err != nil {
		return
	}
	partHeader := make(textproto.MIMEHeader)
	partHeader.Set("Content-Disposition", `form-data; name="entity_content"`)
	partHeader.Set("Content-Type", "application/json")
	if formWriter, err = writer.CreatePart(partHeader); err != nil {
		return
	}

	salesForceFile := struct {
		Filename string `json:"PathOnClient"`
	}{filename}
	if err = json.NewEncoder(formWriter).Encode(&salesForceFile); err != nil {
		return
	}
	writer.Close()

	var rel *url.URL
	if rel, err = url.Parse("/services/data/v46.0/sobjects/ContentVersion"); err != nil {
		return
	}

	var req *http.Request
	if req, err = http.NewRequest(
		http.MethodPost,
		client.BaseURL.ResolveReference(rel).String(),
		bytes.NewReader(buffer.Bytes()),
	); err != nil {
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())

	res = &Response{}
	err = client.Do(req, res)
	return
}
